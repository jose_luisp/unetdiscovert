﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.Net.Sockets;
using System;

public enum Estado
{
    DISCONNECTED,
    HOST,
    CLIENT
}
public class CustomNetworkManager : NetworkManager
{

    public NetworkDiscovery discovery;
    
    NetworkManager netman;
    string playerName = "";



    public Estado state;
    public short numConnectedPlayers = 0;
    public int MaxPlayers = 8; //maxconections accesible desde inspector debug
    public uint SecondsWaiting = 2;

    public NetworkClient networkCliente;

    public GameObject Playerprefab;



    // Use this for initialization
    void Start() {

        Application.runInBackground = true;
        Application.targetFrameRate = 60;


        playerName = "Player" + UnityEngine.Random.Range(1, 1000);
        state = Estado.DISCONNECTED;


        ClientScene.RegisterPrefab(Playerprefab);

        netman = NetworkManager.singleton;
        netman.networkAddress = GetLocalIPAddress().ToString();

        discovery.broadcastData = netman.networkAddress + ":" + netman.networkPort + ":" + playerName;
        Debug.Log("broadcastdata=" + discovery.broadcastData);

        discovery.Initialize();
        StartCoroutine(EstablishConnection());


    }



    IEnumerator EstablishConnection()
    {

        Debug.Log("Establishing connection: Searching for host");
        discovery.StartAsClient();

        yield return new WaitForSeconds(SecondsWaiting);
        if (Estado.DISCONNECTED != state)
        {
            yield break;
        }
        Debug.Log("Establishing connection: No host found, will become host");
        discovery.StopBroadcast();

        netman.StartHost();
        discovery.StartAsServer();


    }


    

    public override void OnStartHost()
    {
        
        state = Estado.HOST;
        base.OnStartHost();

    }

    


    #region Servidor

    public override void OnClientConnect(NetworkConnection conn)
    {
        
        if (!clientLoadedScene)
        {
            ClientScene.Ready(conn);
            Debug.Log("OnClientConnect Server");
            if (netman.autoCreatePlayer == false)
            {

                int id = NetworkClient.allClients[0].connection.playerControllers.Count;
                ClientScene.AddPlayer((short)id);
                

            }
        }

    }


    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {

        Debug.Log("Spawning player" + conn.connectionId);

        if (numConnectedPlayers > netman.maxConnections)
        {

            if (discovery.running == true)
            {

                Debug.Log("Stop Host Broadcast...");
                discovery.StopBroadcast();


            }

        }

        GameObject player = GameObject.Instantiate(Playerprefab, Playerprefab.transform.position, Quaternion.identity) as GameObject;
        player.name = playerName;
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);

       
        numConnectedPlayers++;



    }


    #endregion


    #region Client
    public void conectar(string ip, string port)
    {

        state = Estado.CLIENT;
        netman.networkAddress = ip;

        int tempPort = 0;
        int.TryParse(port, out tempPort);
        netman.networkPort = tempPort;


        Debug.Log("Connecting....");

        networkCliente = new NetworkClient();
        networkCliente.RegisterHandler(MsgType.Connect, ClientConnectedServer);
        networkCliente.Connect(networkAddress, networkPort);
        




    }

    
    void ClientConnectedServer(NetworkMessage msg)
    {
      
        Debug.Log("conectado a " + msg.conn.address );

        ClientScene.Ready(msg.conn);
        int id = NetworkClient.allClients[0].connection.playerControllers.Count;
        ClientScene.AddPlayer((short)id);



    }

   


    #endregion



    IPAddress GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());

        for (ushort i = 0; i < host.AddressList.Length; i++)
        {
            if (host.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
            {
                return host.AddressList[i];
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }


}



