﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class OverNetworkDiscovery : NetworkDiscovery {

    public CustomNetworkManager network;

	// Use this for initialization
	void Start () {
        //para adjuntarlo utilizar el inspector en modo DEBUG
        network = GameObject.FindObjectOfType<CustomNetworkManager>();
        
    }



    public override void OnReceivedBroadcast(string fromAddress, string data)
    {

        if (network.numConnectedPlayers >= network.maxConnections) return;

        if (network.state != Estado.CLIENT)
        {

            network.conectar(fromAddress.Split(':')[3], data.Split(':')[1]);
        }


    }


}
