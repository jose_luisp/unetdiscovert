﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerManager : NetworkBehaviour
{

    public float transformMovementSpeed = 25f;

    void Start()
    {
        Debug.Log("usa las flechas para moverte");

    }

    void Update()
    {
      
        if (!hasAuthority) return;
        
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position = transform.position + new Vector3(0, 0, -1.5f) * transformMovementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position = transform.position + new Vector3(0, 0, 1.5f) * transformMovementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position = transform.position + new Vector3(-1, 0, 0) * transformMovementSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = transform.position + new Vector3(1, 0, 0) * transformMovementSpeed * Time.deltaTime;
        }
       
    }

  

    public override void OnStartLocalPlayer()
    {
        transform.GetComponent<MeshRenderer>().material.color = Color.blue;
    }
}